package com.itheima.rabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String EXCHANGE_NAME = "boot_topic_exchange";
    public static final String QUEUE_NAME = "boot_queue";

    /**
     * 1.交换机
     *
     * @return
     */
    @Bean
    public Exchange bootExchange() {
        return ExchangeBuilder.topicExchange(EXCHANGE_NAME).durable(true).build();
    }


    /**
     * 2.Queue 队列
     *
     * @return
     */
    @Bean
    public Queue bootQueue() {
        return QueueBuilder.durable(QUEUE_NAME).build();
    }

    /**
     * 3. 队列和交互机绑定关系 Binding
     *
     * @param queue    队列
     * @param exchange 交换机
     * @return
     */
    @Bean
    public Binding bindQueueExchange(Queue queue, Exchange exchange) {

        return BindingBuilder.bind(queue).to(exchange).with("boot.#").noargs();
    }


}
